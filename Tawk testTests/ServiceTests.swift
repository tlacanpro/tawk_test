//
//  ServiceTests.swift
//  Tawk testTests
//
//  Created by thomas lacan on 11/04/2021.
//

import XCTest
import CoreData
@testable import Tawk_test

class ServiceTests: XCTestCase {

    lazy var engine = Engine(persistentContainer: persistentContainer)
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Tawk_test")
        container.loadPersistentStores(completionHandler: { (_, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    /// Make sure before tests that DB and all related data are removed + remove cached data
    override func setUp() {
        super.setUp()
        emptyDB()
        engine.usersService.users.removeAll()
        engine.profilesService.profiles.removeAll()
    }
    
    /// Remove all data after test + remove cached data
    override func tearDown() {
        super.tearDown()
        emptyDB()
        engine.usersService.users.removeAll()
        engine.profilesService.profiles.removeAll()
    }

    /// Remove all data in local DB
    func emptyDB() {
        engine.usersService.dbUsers()?.forEach({
            persistentContainer.viewContext.delete($0)
        })
        engine.profilesService.dbProfiles()?.forEach({
            persistentContainer.viewContext.delete($0)
        })
        do {
            try persistentContainer.viewContext.save()
        } catch {
            print("error: \(error)")
        }
    }
}
