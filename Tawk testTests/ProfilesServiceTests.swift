//
//  ProfilesServiceTests.swift
//  Tawk testTests
//
//  Created by thomas lacan on 11/04/2021.
//

import XCTest
import CoreData
@testable import Tawk_test

class ProfilesServiceTests: ServiceTests {
    static let kNoteText = "note"
    
    
    /// Test profile loading from API + storage in DB + cache storage + service status
    func testLoadProfile() {
        let expectation = self.expectation(description: "testProfile")
        XCTAssertTrue(engine.profilesService.dbProfiles()?.isEmpty ?? true)
        XCTAssertTrue(engine.profilesService.profiles.isEmpty)
        engine.usersService.loadUsers(onDone: { [weak self] error in
            XCTAssertNil(error)
            XCTAssertFalse(self?.engine.usersService.users.isEmpty ?? false)
            guard let user = self?.engine.usersService.users.first else {
                return
            }
            self?.engine.profilesService.loadProfileFromUser(user, waitSaveDB: true, onDone: { [weak self](profile, error) in
                XCTAssertNil(error)
                XCTAssertNotNil(profile)
                XCTAssertTrue(profile?.login == user.login)
                XCTAssertTrue(profile == self?.engine.profilesService.profiles.first)
                XCTAssertFalse(self?.engine.profilesService.dbProfiles()?.isEmpty ?? true)
                XCTAssertFalse(self?.engine.profilesService.profiles.isEmpty ?? true)
                XCTAssertTrue(self?.engine.profilesService.state == .loaded)
            })
            expectation.fulfill()
        })
        waitForExpectations(timeout: 100)
    }
    
    /// Test profile note saving + saving in DB
    func testSaveNote() {
        let expectation = self.expectation(description: "testProfileSaveNote")
        XCTAssertTrue(engine.profilesService.dbProfiles()?.isEmpty ?? true)
        XCTAssertTrue(engine.profilesService.profiles.isEmpty)
        engine.usersService.loadUsers(onDone: { [weak self] error in
            XCTAssertNil(error)
            XCTAssertFalse(self?.engine.usersService.users.isEmpty ?? false)
            guard let user = self?.engine.usersService.users.first else {
                return
            }
            self?.engine.profilesService.loadProfileFromUser(user, waitSaveDB: true, onDone: { [weak self](profile, error) in
                XCTAssertNil(error)
                XCTAssertNotNil(profile)
                guard var profile = profile else {
                    return
                }
                XCTAssertTrue(profile.note.isNilOrEmpty)
                profile.note = ProfilesServiceTests.kNoteText
                XCTAssertFalse(profile.note.isNilOrEmpty)
                XCTAssertFalse(self?.engine.profilesService.dbProfiles()?.isEmpty ?? true)
                XCTAssertNil(self?.engine.profilesService.profiles.first(where: { $0.note == ProfilesServiceTests.kNoteText }))
                XCTAssertNil(self?.engine.profilesService.dbProfiles()?.first(where: {
                    $0.note == ProfilesServiceTests.kNoteText
                }))
                self?.engine.profilesService.saveProfile(profile, login: user.login, onDone: {
                    XCTAssertNotNil(self?.engine.profilesService.profiles.first(where: { $0.note == ProfilesServiceTests.kNoteText }))
                    XCTAssertNotNil(self?.engine.profilesService.dbProfiles()?.first(where: {
                        $0.note == ProfilesServiceTests.kNoteText
                    }))
                    expectation.fulfill()
                })
            })
        })
        waitForExpectations(timeout: 300)
    }
}
