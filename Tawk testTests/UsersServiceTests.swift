//
//  UsersServiceTests.swift
//  Tawk testTests
//
//  Created by thomas lacan on 10/04/2021.
//

import XCTest
import CoreData
@testable import Tawk_test

class UserServiceTests: ServiceTests {
    
    /// Test users loading from API + storage in DB + cache storage + service status
    func testLoadUsers() {
        let expectation = self.expectation(description: "testLoadUsers")
        XCTAssertTrue(engine.usersService.dbUsers()?.isEmpty ?? true)
        XCTAssertTrue(engine.usersService.users.isEmpty)
        engine.usersService.loadUsers(onDone: { [weak self] error in
            XCTAssertNil(error)
            XCTAssertFalse(self?.engine.usersService.users.isEmpty ?? true)
            XCTAssertFalse(self?.engine.usersService.dbUsers()?.isEmpty ?? true)
            XCTAssertTrue(self?.engine.usersService.state == .loaded)
            expectation.fulfill()
        })
        waitForExpectations(timeout: 100)
    }

}
