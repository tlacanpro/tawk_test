//
//  main.swift
//  Tawk test
//
//  Created by thomas lacan on 11/04/2021.
//

import Foundation
import UIKit

private func delegateClassName() -> String? {
   NSClassFromString("XCTestCase") == nil ? NSStringFromClass(AppDelegate.self) : nil
}

_ = UIApplicationMain(
        CommandLine.argc,
        CommandLine.unsafeArgv,
        nil,
        delegateClassName()
    )
