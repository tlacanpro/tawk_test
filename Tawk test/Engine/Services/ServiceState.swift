//
//  ServiceState.swift
//  Tawk test
//
//  Created by thomas lacan on 05/04/2021.
//

import Foundation

public enum ServiceState: Equatable {
    case idle
    case loading
    case loaded
    case error(ApiError)
    
    /// related error description
    /// - Returns: debug error description
    public func localizedDescription() -> String {
        switch self {
        case .error(let apiError):
            return apiError.localizedDescription
        default:
            return "Not an Error"
        }
    }
    
    public static func == (rhs: ServiceState, lhs: ServiceState) -> Bool {
        switch (rhs, lhs) {
        case (.idle, .idle),
             (.loading, .loading),
             (.loaded, .loaded),
             (.error, .error):
            return true
        default:
            return false
        }
    }
}
