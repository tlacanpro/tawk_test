//
//  ProfilesService.swift
//  Tawk test
//
//  Created by thomas lacan on 06/04/2021.
//

import Foundation
import UIKit
import CoreData

class ProfilesService {
    static let kEntityName = "GithubProfileModelDB"
    
    var profiles: [GithubProfileModel]
    let networkClient: NetworkClient
    let persistentContainer: NSPersistentContainer
    var state: ServiceState = .idle {
        didSet {
            print("[ProfilesUserService] State changed to \(state)")
        }
    }
    
    init(networkClient: NetworkClient, persistentContainer: NSPersistentContainer) {
        self.persistentContainer = persistentContainer
        self.networkClient = networkClient
        self.profiles = []
        dbProfiles()?.forEach({ profiles.append($0.githubProfileModel()) })
    }
    
    /// List of profiles containing text (case insensitve) in note
    /// - Parameter text: text to look for
    /// - Returns: list of mathing profiles
    func profilesWithNoteContaining(_ text: String) -> [GithubProfileModel] {
        profiles.filter({ ($0.note?.lowercased().contains(text.lowercased() )) ?? false })
    }
    
    /// Related profile for user
    /// - Parameter user: user to check
    /// - Returns: related profile if it exists
    func profileForUser(_ user: GithubUserModel) -> GithubProfileModel? {
        profiles.first(where: { $0.login == user.login })
    }
    
    /// List of profiles into DB
    /// - Returns: list of profiles
    func dbProfiles() -> [GithubProfileModelDB]? {
        let fetchRequest = NSFetchRequest<GithubProfileModelDB>(entityName: ProfilesService.kEntityName)
        do {
            return try persistentContainer.viewContext.fetch(fetchRequest)
        } catch {
            print("error: \(error)")
            return nil
        }
    }
    
    /// Save profile into local DB
    /// - Parameters:
    ///   - profile: profile to save
    ///   - login: related profile user login
    ///   - onDone: completion block use when db operation completed, use for unit testing
    func saveProfile(_ profile: GithubProfileModel, login: String?, onDone: (() -> Void)? = nil) {
        guard let login = login else { return }
        var newProfile = profile
        newProfile.login = login
        profiles.removeAll { profile in profile.login == login }
        profiles.append(newProfile)
        
        persistentContainer.performBackgroundTask({ context in
            let fetchRequest = NSFetchRequest<GithubProfileModelDB>(entityName: ProfilesService.kEntityName)
            let dbProfiles = try? context.fetch(fetchRequest)
            dbProfiles?.filter({ $0.login == login }).forEach({
                context.delete($0)
            })
            guard let profileDB = NSEntityDescription.insertNewObject(forEntityName: ProfilesService.kEntityName,
                                        into: context)
                            as? GithubProfileModelDB else { return }
            profileDB.fill(with: newProfile)
            do {
                try context.save()
                onDone?()
            } catch {
                print("error: \(error)")
            }
        })
    }
    
    /// Load from API related profile for user
    /// - Parameters:
    ///   - user: user to load profile
    ///   - waitSaveDB: Use for unit testing to wait DB operations completed
    ///   - onDone: load completion handler with possible api error and related profile
    func loadProfileFromUser(_  user: GithubUserModel, waitSaveDB: Bool = false, onDone: ((GithubProfileModel?, ApiError?) -> Void)? = nil) {
        networkClient.queue.async { [weak self] in
            self?.state = .loading
            self?.networkClient.call(endPoint: .profile, urlDict: [":login": user.login]) { [weak self](asyncResult) in
                switch asyncResult {
                    case .success(let data, let code):
                        if code == 200, let data = data, let profile: GithubProfileModel = data.modelFromData() {
                            if waitSaveDB {
                                self?.saveProfile(profile, login: user.login, onDone: { [weak self] in
                                    self?.state = .loaded
                                    onDone?(profile, nil)
                                    self?.networkClient.semaphore.signal()
                                })
                                return
                            }
                            self?.saveProfile(profile, login: user.login)
                            self?.state = .loaded
                            onDone?(profile, nil)
                            self?.networkClient.semaphore.signal()
                            return
                        } else {
                            self?.state = .error(ApiError.unexpectedApiResponse)
                            onDone?(nil, ApiError.unexpectedApiResponse)
                            self?.networkClient.semaphore.signal()
                        }
                    case .error(let error):
                        print("[Profile] error \(error?.localizedDescription ?? "unknown error")")
                        self?.state = .error(ApiError.from(error: error))
                        onDone?(nil, ApiError.from(error: error))
                        self?.networkClient.semaphore.signal()
                }
            }
            self?.networkClient.semaphore.wait()
        }
    }
}
