//
//  ReachabilityService.swift
//  Tawk test
//
//  Created by thomas lacan on 08/04/2021.
//

import Foundation
import Combine

class ReachabilityService: ObservableObject {
    
    // MARK: - Properties
    
    @Published var isConnected: Bool = true
    
    private let reachabilityInstance = Reachability(hostname: "google.com")
    private var subscription: AnyCancellable? = nil {
        willSet {
            subscription?.cancel()
        }
    }
    
    // MARK: - Destruction
    
    deinit {
        reachabilityInstance?.stopNotifier()
    }
    
    // MARK: - Initialization
    
    init() {
        do {
            try reachabilityInstance?.startNotifier()
        } catch {
            print("startNotifier failure")
        }
        subscription = NotificationCenter.default.publisher(for: Notification.Name.reachabilityChanged)
                        .compactMap({ $0.object as? Reachability })
                        .map({ $0.connection != .none })
                        .assign(to: \.isConnected, on: self)
    }
}
