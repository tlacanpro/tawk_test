//
//  ImageService.swift
//  Tawk test
//
//  Created by thomas lacan on 06/04/2021.
//

import Foundation
import UIKit

class ImageService {
    let networkClient: NetworkClient
    
    init(networkClient: NetworkClient) {
        self.networkClient = networkClient
    }
    
    /// Load Image from url
    /// - Parameters:
    ///   - url: image url
    ///   - onDone: Image loading completion handler with related, data, UIImage from data, possible error description
    /// - Returns: related url operation to cancel it if needed
    @discardableResult
    func image(url: URL, onDone: @escaping (Data?, UIImage?, String?) -> Void) -> URLSessionDataTask? {
        return networkClient.call(url: url, verb: .get, dict: nil, urlDict: nil, headers: nil) { (asyncResult) in
            switch asyncResult {
            case .success(let data, _):
                if let data = data {
                    onDone(data, UIImage(data: data), nil)
                    return
                }
                onDone(nil, nil, nil)
            case .error(let error):
                print("[ImageService] error \(String(describing: error))")
                onDone(nil, nil, error?.localizedDescription)
            }
        }
    }
}
