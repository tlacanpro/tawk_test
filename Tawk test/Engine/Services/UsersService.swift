//
//  UsersService.swift
//  Tawk test
//
//  Created by thomas lacan on 05/04/2021.
//

import Foundation
import UIKit
import CoreData

protocol UsersServiceObserver {
    /// Notify observers that service state changed
    /// - Parameters:
    ///   - usersService: service
    ///   - state: new state
    func onUsersService(usersService: UsersService, didUpdate state: ServiceState)
}

class UsersService {
    static let kEntityName = "GithubUserModelDB"
    
    var users: [GithubUserModel]
    fileprivate (set) var observers = WeakObserverOrderedSet<UsersServiceObserver>()
    let networkClient: NetworkClient
    let persistentContainer: NSPersistentContainer
    var state: ServiceState = .idle {
        didSet {
            print("[UsersService] State changed to \(state)")
        }
    }
    
    init(networkClient: NetworkClient, persistentContainer: NSPersistentContainer) {
        self.persistentContainer = persistentContainer
        self.networkClient = networkClient
        self.users = []
        dbUsers()?.forEach({ users.append($0.githubUserModel()) })
        users.sort(by: { $0.id < $1.id })
        loadUsers()
    }
    
    /// Add user service observer to events
    /// - Parameter observer: User Observer
    func register(observer: UsersServiceObserver) {
        observers.add(value: observer)
    }
    
    /// Remove user service observer to events
    /// - Parameter observer: User Observer
    func unregister(observer: UsersServiceObserver) {
        observers.remove(value: observer)
    }
    
    
    /// Save user into local DB
    /// - Parameters:
    ///   - user: user to save
    ///   - saveContext: must save context after adding user
    func saveUser(_ user: GithubUserModel) {
        persistentContainer.performBackgroundTask({ context in
            guard let userDB = NSEntityDescription.insertNewObject(forEntityName: UsersService.kEntityName,
                                                                   into: context) as? GithubUserModelDB  else {
                return
            }
            userDB.fill(with: user)
            
            try? context.save()
        })
    }
    
    
    /// List of users in local DB
    /// - Returns: array of DB user
    func dbUsers() -> [GithubUserModelDB]? {
        let fetchRequest = NSFetchRequest<GithubUserModelDB>(entityName: UsersService.kEntityName)
        //let sort = NSSortDescriptor(key: "id", ascending: true)
        //fetchRequest.sortDescriptors = [sort]
        
        do {
            return try persistentContainer.viewContext.fetch(fetchRequest)
        } catch {
            print("error: \(error)")
            return nil
        }
    }
    
    
    /// Save users array into local DB
    /// - Parameters:
    ///   - users: array of users to save
    ///   - clearDB: Clear local DB before adding users
    func saveUsers(users: [GithubUserModel], clearDB: Bool) {
        if clearDB {
            persistentContainer.performBackgroundTask({ [weak self] context in
                self?.dbUsers()?.forEach({ context.delete($0) })
                try? context.save()
            })
        }
        users.forEach({ saveUser($0) })
    }
    
    /// Load users from API
    /// - Parameter onDone: load completion handler with possible api error
    func loadUsers(onDone: ((ApiError?) -> Void)? = nil) {
        networkClient.queue.async { [weak self] in
            self?.state = .loading
            let idParameter = self?.users.last?.id ?? 0
            self?.networkClient.call(endPoint: .users, urlDict: ["since": idParameter]) { [weak self](asyncResult) in
                switch asyncResult {
                    case .success(let data, let code):
                        if code == 200 {
                            if let data = data, let users: [GithubUserModel] = data.arrayModelFromData() {
                                self?.users.append(contentsOf: users)
                                self?.saveUsers(users: users, clearDB: idParameter == 0)
                                self?.state = .loaded
                                onDone?(nil)
                                self?.networkClient.semaphore.signal()
                                return
                            }
                        }
                        self?.state = .error(ApiError.unexpectedApiResponse)
                        onDone?(ApiError.unexpectedApiResponse)
                        self?.networkClient.semaphore.signal()
                    case .error(let error):
                        print("[User] error \(error?.localizedDescription ?? "unknown error")")
                        self?.state = .error(ApiError.from(error: error))
                        onDone?(ApiError.from(error: error))
                        self?.networkClient.semaphore.signal()
                }
            }
            self?.networkClient.semaphore.wait()
        }
    }
}
