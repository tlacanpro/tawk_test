//
//  WeakObserverOrderedSet.swift
//  Tawk test
//
//  Created by thomas lacan on 07/04/2021.
//

import Foundation

struct WeakObserverOrderedSet<T> {
    
    struct Weak {
        weak var value: AnyObject?
        init(_ value: AnyObject) {
            self.value = value
        }
    }
    
    var elements: [Weak] = []
    
    /// Index in set of observer
    /// - Parameter value: observer to find
    /// - Returns: index value
    private func indexOf(value: AnyObject) -> Int? {
        for (idx, val) in elements.enumerated() where val.value === value {
            return idx
        }
        return nil
    }
    
    /// Add observer into set if needed
    /// - Parameter value: An observer
    mutating func add(value: T) {
        let anyValue = value as AnyObject
        guard indexOf(value: anyValue) == nil else { return }
        elements.append(Weak(anyValue))
    }
    
    /// Add observer into set if needed
    /// - Parameter value: An observer
    mutating func remove(value: T) {
        let anyValue = value as AnyObject
        guard let i = indexOf(value: anyValue) else { return }
        elements.remove(at: i)
    }
    
    /// Remove released observers (View controllers)
    mutating func removeReleasedObservers() {
        elements = elements.filter { $0.value != nil}
    }
    
    
    /// Call function into oberservers
    /// - Parameter function: function to invoke to observers
    func invoke(_ function: ((T) -> Void)) {
        for elem in elements {
            if let eVal = elem.value as? T {
                function(eVal)
            }
        }
    }
}
