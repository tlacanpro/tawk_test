//
//  NetworkClient.swift
//  Tawk test
//
//  Created by thomas lacan on 05/04/2021.
//

import Foundation

enum HTTPVerb: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
    case patch = "PATCH"
}

enum AsyncCallResult<T> {
    case success(data: T?, httpStatus: Int)
    case error(error: Error?)
}

enum Server: String {
    case dev = "https://api.github.com/"
}

class NetworkClient {
    static let kResponseError = "error"
    static let kResponseSuccess = "ok"
    static let kResponseStatus = "state"
    
    lazy var server: Server = .dev
    
    let queue = DispatchQueue(label: "NetworkQueue")
    let semaphore = DispatchSemaphore(value: 0)
    
    enum ParameterEncoding {
        case JSON
        case URL
    }
    var engine: Engine?
    
    let endpointMapperClass: EndpointMapper.Type
    init(endpointMapperClass: EndpointMapper.Type) {
        self.endpointMapperClass = endpointMapperClass
    }
    
    // MARK: - HTTP Methods
    /// call end point with provided options
    /// - Parameters:
    ///   - endPoint: end point to call
    ///   - dict: dictionary with params
    ///   - urlDict: dictionary with url params
    ///   - headers: custom headers
    ///   - timeout: call time out
    ///   - authenticated: need to be authenticated to call endpoint
    ///   - ignoreContainedKey: ignore key in url remplacement
    ///   - onDone: completion handler with call result
    /// - Returns: url session task
    @discardableResult
    public func call(endPoint: ApiEndpoint, dict: [String: Any?]? = nil, urlDict: [String: Any?]? = nil, headers: [String: String]? = nil, timeout: TimeInterval = 60, authenticated: Bool = false, ignoreContainedKey: Bool = false, onDone: @escaping (AsyncCallResult<Data>) -> Void) -> URLSessionDataTask? {
        guard let url = urlFor(endPoint: endPoint) else {
            return nil
        }
        let method = endpointMapperClass.method(for: endPoint)
        return call(url: url, verb: method, dict: dict, urlDict: urlDict,
                    headers: headers, timeout: timeout, authenticated: authenticated,
                    ignoreContainedKey: ignoreContainedKey, onDone: onDone)
    }
    
    /// URL for provided endpoint
    /// - Parameter endPoint: end point
    /// - Returns: url for end point
    func urlFor(endPoint: ApiEndpoint) -> URL? {
        let string = server.rawValue + endpointMapperClass.path(for: endPoint).trimmingCharacters(in: .whitespacesAndNewlines)
        return URL(string: string)
    }
    
    /// call url with provided options
    /// - Parameters:
    ///   - url: url to call
    ///   - verb: HTTP verb
    ///   - dict: dictionary with params
    ///   - urlDict: dictionary with url params
    ///   - headers: custom headers
    ///   - timeout: call time out
    ///   - authenticated: need to be authenticated to call endpoint
    ///   - ignoreContainedKey: ignore key in url remplacement
    ///   - onDone: completion handler with call result
    /// - Returns: url session task
    @discardableResult
    // swiftlint:disable:next function_parameter_count
    func call(url: URL, verb: HTTPVerb, dict: [String: Any?]?, urlDict: [String: Any?]?, headers: [String: String]?, timeout: TimeInterval = 30, authenticated: Bool = false, ignoreContainedKey: Bool = false, onDone: @escaping (AsyncCallResult<Data>) -> Void) -> URLSessionDataTask {
        
        var data: Data?
        var encodedURL: URL = url
        if let dict = dict {
            data = try? JSONSerialization.data(withJSONObject: dict, options: [])
        }
        if let urlDict = urlDict {
            encodedURL = NetworkClient.url(with: encodedURL, urlParams: urlDict)
        }
        
        return call(url: encodedURL, verb: verb, data: data, headers: headers, timeout: timeout, authenticated: authenticated, onDone: onDone)
    }

    /// call url with provided options
    /// - Parameters:
    ///   - url: url to call
    ///   - verb: HTTP verb
    ///   - data: http body data
    ///   - headers: custom headers
    ///   - timeout: call time out
    ///   - authenticated: need to be authenticated to call endpoint
    ///   - onDone: completion handler with call result
    /// - Returns: url session task
    // swiftlint:disable:next function_parameter_count
    private func call(url: URL, verb: HTTPVerb, data: Data?, headers: [String: String]?, timeout: TimeInterval, authenticated: Bool = false, onDone: @escaping (AsyncCallResult<Data>) -> Void) -> URLSessionDataTask {
        var request = URLRequest(url: url)
        request.httpMethod = verb.rawValue
        
        var allHeaders: [String: String] = [
            "content-type": "application/json",
            "Accept": "application/json"
        ]
        /*if authenticated, let token = sessionService.currentSession?.token {
            allHeaders["Authentication"] = token
        }*/
        if let headers = headers {
            headers.forEach { allHeaders[$0] = $1 }
        }
        request.allHTTPHeaderFields = allHeaders
        request.httpBody = data
        request.timeoutInterval = timeout
        
        return call(request: request, onDone: onDone)
    }
    
    /// call url request
    /// - Parameters:
    ///   - request: url request
    ///   - onDone: completion handler with call result
    ///   - refreshTokenOnUnauthorizedError: manage token refresh if needed
    /// - Returns: url session task
    @discardableResult
    public func call(request: URLRequest, onDone: @escaping (AsyncCallResult<Data>) -> Void, refreshTokenOnUnauthorizedError: Bool = false) -> URLSessionDataTask {
        let task = URLSession.shared.dataTask(with: request) { [weak self] data, response, error in
            guard let data = data, error == nil, let urlResponse = response as? HTTPURLResponse else {
                let error = ApiError.from(error: error)
                self?.dispatchToMainThread(asyncCallResult: .error(error: error), block: onDone)
                return
            }
            
            print(
                """
                [NetworkClient]
                \(request.httpMethod ?? "GET") \(request.url?.absoluteString ?? ""),
                headers: \(request.allHTTPHeaderFields?.description ?? "[]"),
                payload: \((try? request.httpBody?.mapJSON()) ?? "[]")
                cUrl Request:
                \(request.curlString)
                -->
                Response \(urlResponse.statusCode) \(response?.description ?? "")
                Response Data \((try? data.mapJSON()) ?? "")
                Response JSON \(String(data: data, encoding: .utf8) ?? "")

                """
            )
            
            self?.dispatchToMainThread(asyncCallResult: .success(data: data, httpStatus: urlResponse.statusCode), block: onDone)
        }
        task.resume()
        return task
    }
    
    // MARK: - Thread Helper
    /// handle call result on main thread
    /// - Parameters:
    ///   - asyncCallResult: call result
    ///   - block: completion block to handle
    private func dispatchToMainThread<T>(asyncCallResult: AsyncCallResult<T>, block: @escaping (AsyncCallResult<T>) -> Void) {
        if Thread.current == Thread.main {
            block(asyncCallResult)
        } else {
            DispatchQueue.main.async {
                block(asyncCallResult)
            }
        }
    }
    
    // MARK: - Parsing Helpers
    /// Manage parameters into url
    /// - Parameters:
    ///   - url: parameters to handle
    ///   - dict: dictionaries with parameters values
    ///   - ignoreContainedKey: true if want to change values in url
    /// - Returns: url with parameters
    static func url(with url: URL, urlParams dict: [String: Any?], ignoreContainedKey: Bool = false) -> URL {
        var urlString = url.absoluteString
        let params = dict.filter { (key: String, _: Any?) -> Bool in
            ignoreContainedKey ? true : !urlString.contains(key)
        }
        
        if !ignoreContainedKey {
            for (key, value) in dict where urlString.contains(key) {
                if let value = value as? String {
                    urlString = urlString.replacingOccurrences(of: key, with: value)
                } else {
                    let stringValue = "\(value ?? "")"
                    urlString = urlString.replacingOccurrences(of: key, with: stringValue)
                }
            }
        }
        
        var urlComponents = URLComponents(string: urlString)
        let queryItems = params.flatMap({ (key, value) -> [URLQueryItem] in
            if let value = value as? String {
                return [ URLQueryItem(name: key, value: value) ]
            } else if let value = value as? [String] {
                return value.compactMap({ string -> URLQueryItem? in
                    return URLQueryItem(name: "\(key)[]", value: string)
                })
            } else if let value = value {
                return [ URLQueryItem(name: key, value: "\(value)") ]
            }
            return []
        })
        
        urlComponents?.queryItems = queryItems
        return urlComponents?.url ?? url
    }
}
