//
//  Endpoint.swift
//  Tawk test
//
//  Created by thomas lacan on 05/04/2021.
//

import Foundation

protocol EndpointMapper {
    static func path(for endpoint: ApiEndpoint) -> String
    static func method(for endpoint: ApiEndpoint) -> HTTPVerb
}

enum ApiEndpoint {
    case users
    case profile
}

struct TawkEndpointMapper: EndpointMapper {
    
    /// Path for end point
    /// - Parameter endpoint: endpoint in API
    /// - Returns: end point related path
    static func path(for endpoint: ApiEndpoint) -> String {
        switch endpoint {
        case .users: return "users"
        case .profile: return "users/:login"
        }
    }
    
    /// HTTP method for API end point
    /// - Parameter endpoint: endpoint in API
    /// - Returns: HTTP verb
    static func method(for endpoint: ApiEndpoint) -> HTTPVerb {
        switch endpoint {
        case .users, .profile: return .get
        }
    }
}
