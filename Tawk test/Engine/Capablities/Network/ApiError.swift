//
//  ApiError.swift
//  Tawk test
//
//  Created by thomas lacan on 05/04/2021.
//

import Foundation

public enum ApiError: Error {
    case noNetwork
    case networkError
    case notConnected
    case decodingError
    case unexpectedApiResponse
    case accessUnauthorized
    case other(error: Error?)
    
    
    /// Api error from Error
    /// - Parameter error: error to convert
    /// - Returns: Api error
    public static func from(error: Error?) -> ApiError {
        if let error = error as? ApiError {
            return error
        }
        if let nserror = error as NSError? {
            if nserror.code == NSURLErrorNotConnectedToInternet {
                return .noNetwork
            }
        }
        return .other(error: error)
    }
}
