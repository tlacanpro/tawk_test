//
//  RemoteImageDataStore.swift
//  Tawk test
//
//  Created by thomas lacan on 06/04/2021.
//

import Foundation
import UIKit

class RemoteImageDataStore: NSObject {
    
    let dataStore: FileDataStore
    
    let cache = NSCache<NSString, AnyObject>()
    let imageService: ImageService
    
    init(imageService: ImageService, dataStore: FileDataStore) {
        self.imageService = imageService
        self.dataStore = dataStore
        super.init()
        
        try? FileManager.default.createDirectory(atPath: cacheDirectory(), withIntermediateDirectories: true, attributes: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onMemoryWarningReceived),
            name: UIApplication.didReceiveMemoryWarningNotification,
            object: nil
        )
        
        purgeExpiredCachedFiles()
    }
    
    
    /// In case of memory warning delete cache images
    @objc func onMemoryWarningReceived() {
        print("[RemoteImageDataStore] onMemoryWarningReceived, deleting cache")
        cache.removeAllObjects()
    }
    
    /// Cache images directory
    /// - Returns: directory
    func cacheDirectory() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)
        return (paths[0] as NSString).appendingPathComponent("images")
    }
    
    
    /// Full file path in cache
    /// - Parameter fileName: the file name
    /// - Returns: file path
    func filePathFromCache(fileName: String) -> String {
        return (cacheDirectory() as NSString).appendingPathComponent(fileName)
    }
    
    
    /// If some images are too old into cache (30 days) remove them
    func purgeExpiredCachedFiles() {
        let directory = cacheDirectory()
        
        let directoryURL = URL(fileURLWithPath: directory, isDirectory: true)
        let enumerator = FileManager.default.enumerator(
            at: directoryURL,
            includingPropertiesForKeys: [.creationDateKey],
            options: [.skipsSubdirectoryDescendants, .skipsHiddenFiles],
            errorHandler: { (_, error) -> Bool in
                print("[RemoteImageDataStore]  Error deleting old files \(directory)\(error)")
                return true
            })
        let limitDate = Date().addingTimeInterval(-30 * 24 * 60 * 60) // 30days
        while let url = enumerator?.nextObject() as? URL {
            if  let resources = try? url.resourceValues(forKeys: [.creationDateKey]),
                let creationDate = resources.creationDate {
                if creationDate < limitDate {
                    do {
                        try FileManager.default.removeItem(at: url)
                        print("[RemoteImageDataStore]  Cleaned file at \(url)")
                    } catch let error {
                        print("[RemoteImageDataStore]  Error deleting files at \(url)\(error)")
                    }
                } else {
                    //print("[RemoteImageDataStore]  Cache still valid for \(url)")
                }
            } else {
                print("[RemoteImageDataStore]  Error while getting creation date for fileurl \(url)")
            }
        }
    }
    
    /// Get image for url in cache or download it
    /// - Parameters:
    ///   - url: image url to load
    ///   - completionHandler: completion handler wih related image
    func image(for url: URL, completionHandler: @escaping (_ image: UIImage?, _ url: URL) -> Void) {
        let urlHash = url.absoluteString.sha256()
        
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let strongSelf = self else { return }
            let data: NSData? = strongSelf.cache.object(forKey: urlHash as NSString) as? NSData
            
            // get image from nscache if possible
            if let goodData = data {
                let image = UIImage(data: goodData as Data)
                DispatchQueue.main.async {
                    print("[RemoteImageDataStore] Returning image at \(url) from cache")
                    completionHandler(image, url)
                }
                return
            }
            
            // check image available on disk
            let cacheFilePath = strongSelf.filePathFromCache(fileName: urlHash)
            if strongSelf.dataStore.fileExists(at: cacheFilePath) {
                if let data = try? strongSelf.dataStore.data(at: URL(fileURLWithPath: cacheFilePath)) {
                    DispatchQueue.main.async {
                        print("[RemoteImageDataStore] Returning image at \(url) from disk")
                        completionHandler(UIImage(data: data), url)
                    }
                    strongSelf.cache.setObject(data as AnyObject, forKey: urlHash as NSString)
                    return
                }
                strongSelf.dataStore.deleteFileIfExists(at: cacheFilePath)
            }
            completionHandler(nil, url)
        }
    }
    
    func saveImage(_ image: UIImage, from url: URL) {
        let urlHash = url.absoluteString.sha256()
        let cacheFilePath = filePathFromCache(fileName: urlHash)
        guard let data = image.pngData() else { return }
        
        cache.setObject(data as AnyObject, forKey: urlHash as NSString)
        do {
            try dataStore.persist(data: data, at: URL(fileURLWithPath: cacheFilePath))
        } catch let error {
            print("[RemoteImageDataStore]  Error persisting \(urlHash) mathing url \(url.absoluteString) \(error)")
        }
    }
    
    /// Download image at url
    /// - Parameters:
    ///   - url: image url to load
    ///   - store: save image in cache
    ///   - completionHandler: completion handler wih related image
    @discardableResult
    func downloadImageForUrl(_ url: URL, store: Bool = true, completionHandler: @escaping (_ image: UIImage?, _ url: URL) -> Void) -> URLSessionDataTask? {
        imageService.image(url: url, onDone: { [weak self] (_, image, _) in
            
            if let image = image {
                DispatchQueue.main.async {
                    print("[RemoteImageDataStore] Returning image at \(url) from network")
                    completionHandler(image, url)
                }
                if !store {
                    return
                }
                self?.saveImage(image, from: url)
            }
            completionHandler(nil, url)
        })
    }
}
