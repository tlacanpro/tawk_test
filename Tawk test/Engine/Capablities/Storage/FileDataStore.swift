//
//  FileDataStore.swift
//  Tawk test
//
//  Created by thomas lacan on 06/04/2021.
//

import Foundation

class FileDataStore {
    let testEnv: Bool
    
    init(testEnv: Bool = false) {
        self.testEnv = testEnv
        print("[FileDataStore] Using directory \(rootDirectory())")
    }
    
    /// Store Codable into local file
    /// - Parameters:
    ///   - codable: codable object to store
    ///   - filename: file name
    ///   - directory: directory name
    /// - Returns: data writted
    @discardableResult
    func persist<T: Codable>(codable: T, filename: String, in directory: String) -> String? {
        let encoder = JSONEncoder()
        do {
            let encoded = try encoder.encode(codable)
            do {
                return try persist(data: encoded, in: directory, filename: filename)
            } catch let error {
                print("[FileDatastore] Error persisting \(codable) to \(filename)) \(error)")
                return nil
            }
        } catch let error {
            print("[FileDatastore] Unable to encode \(codable) -> \(error)")
            return nil
        }
    }
    
    /// Codable contained into related file
    /// - Parameters:
    ///   - filename: file name
    ///   - directory: directory name
    /// - Returns: Codable instance
    func codable<T: Codable>(from filename: String, in directory: String) -> T? {
        do {
            let data = try self.data(
                from: directory,
                filename: filename
            )
            let decoder = JSONDecoder()
            if let decoded = try? decoder.decode(T.self, from: data) {
                return decoded
            }
            return nil
        } catch let error {
            print("[FileDatastore] Error reading data from \(filename) -> \(error)")
            return nil
        }
    }
    
    /// Codable contained into related url
    /// - Parameter url: url value
    /// - Returns: Codable instance
    func codable<T: Codable>(url: URL) -> T? {
        do {
            let data = try self.data(at: url)
            let decoder = JSONDecoder()
            if let decoded = try? decoder.decode(T.self, from: data) {
                return decoded
            }
            return nil
        } catch let error {
            print("[FileDatastore] Error reading data from \(url.absoluteString) -> \(error)")
            return nil
        }
    }
    
    /// Delete file at path
    /// - Parameter path: path value
    func deleteFileIfExists(at path: String) {
        guard fileExists(at: path) else { return }
        do {
            try deleteFile(at: path)
        } catch let error {
            print("[FileDataStore] Error deleting file at \(path) : \(error)")
        }
    }
    
    
    /// Delete file with related name / directory
    /// - Parameters:
    ///   - directory: directory name
    ///   - filename: fine name
    func deleteFileIfExists(in directory: String, filename: String) {
        deleteFileIfExists(at: filePath(in: directory, filename: filename))
    }
    
    /// App root directory
    /// - Returns: directory value
    func rootDirectory() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(testEnv ? .documentDirectory : .applicationSupportDirectory, .userDomainMask, true)
        return (paths[0] as NSString).appendingPathComponent(testEnv ? "testData" : "")
    }
    
    
    // MARK: - Private
    /// Store Codable data into local file
    /// - Parameters:
    ///   - data: codable data
    ///   - directory: directory name
    ///   - filename: file name
    /// - Throws: writting errors
    /// - Returns: data writted
    @discardableResult
    internal func persist(data: Data, in directory: String, filename: String) throws -> String {
        try FileManager.default.createDirectory(atPath: directory, withIntermediateDirectories: true, attributes: nil)
        let path = filePath(in: directory, filename: filename)
        try persist(data: data, at: URL(fileURLWithPath: path))
        print("[FileDataStore] Data persisted to \(path)")
        return path
    }
    
    /// File data into provided file
    /// - Parameters:
    ///   - directory: directory name
    ///   - filename: file name
    /// - Throws: readding errors
    /// - Returns: file data
    internal func data(from directory: String, filename: String) throws -> Data {
        let url = URL(fileURLWithPath: filePath(in: directory, filename: filename))
        return try data(at: url)
    }
    
    /// Write data at url
    /// - Parameters:
    ///   - data: data to write
    ///   - fileUrl: url where to write
    /// - Throws: writting errors
    func persist(data: Data, at fileUrl: URL) throws {
        try data.write(to: fileUrl, options: [.atomic, .completeFileProtection])
    }
    
    
    /// Data at file url
    /// - Parameter fileUrl: file path url
    /// - Throws: reading error
    /// - Returns: file data
    func data(at fileUrl: URL) throws -> Data {
        return try Data(contentsOf: fileUrl)
    }
    
    /// File related path from directory filename
    /// - Parameters:
    ///   - directory: directory name
    ///   - filename: file name
    /// - Returns: path related to parameters
    func filePath(in directory: String, filename: String) -> String {
        return (directory as NSString).appendingPathComponent(filename)
    }
    
    /// There is at a file at path
    ///   - path: path to check
    /// - Returns: there is a file
    func fileExists(at path: String) -> Bool {
        FileManager.default.fileExists(atPath: path)
    }
    
    /// Check file with name is in directory
    /// - Parameters:
    ///   - directory: directory name
    ///   - filename: file name
    /// - Returns: file is there
    internal func fileExists(in directory: String, filename: String) -> Bool {
        fileExists(at: filePath(in: directory, filename: filename))
    }
    
    
    /// Delete file at path
    /// - Parameter path: file path
    /// - Throws: file deletion error
    private func deleteFile(at path: String) throws {
        try FileManager.default.removeItem(atPath: path)
    }
    
    /// Delete file with name in directory
    /// - Parameters:
    ///   - directory: directory name
    ///   - filename: file name
    /// - Returns: file deletion error
    private func deleteFile(from directory: String, filename: String) throws {
        try deleteFile(at: filePath(in: directory, filename: filename))
    }
}
