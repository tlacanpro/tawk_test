//
//  LoadingIndicatorManager.swift
//  Tawk test
//
//  Created by thomas lacan on 07/04/2021.
//

import Foundation

import UIKit

// MARK: - LoadingIndicatorConstants

private struct LoadingIndicatorConstants {
    static let animationDuration: TimeInterval = 0.5
    static let padding: CGFloat = 25
    static let containerCornerRadius: CGFloat = 10
}

// MARK: - LoadingIndicator

final class LoadingIndicatorManager: UIView, ObservableObject {
    
    // MARK: - Properties
    
    private lazy var container: UIView = {
        let container = UIView()
        container.backgroundColor = .clear
        container.layer.cornerRadius = LoadingIndicatorConstants.containerCornerRadius
        return container
    }()
    
    private lazy var label: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        label.textAlignment = .center
        return label
    }()
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        UIActivityIndicatorView(style: .large)
    }()
    
    @Published var displayCount: Int = 0
    private var backupTimer: Timer?
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
        setupConstraints()
    }
    
    // MARK: - Private Methods
    
    private var keyWindow: UIWindow? {
        return UIApplication.shared.windows.first(where: { $0.isKeyWindow })
    }
    
    /// Setup ui subviews
    private func setupUI() {
        backgroundColor = UIColor.white.withAlphaComponent(0.9)
        isUserInteractionEnabled = true
        addActivityIndicator()
        addLoadingLabel()
        addSubview(container)
        alpha = 0.0
        frame = CGRect(x: 0, y: 0, width: Constants.screenWidth, height: Constants.screenHeight)
    }
    
    /// Add indicator activity into view
    func addActivityIndicator() {
        container.addSubview(activityIndicator)
        activityIndicator.addCenterVerticallyConstraint()
        activityIndicator.addCenterHorizontallyConstraint()
    }
    
    /// Add loading label
    func addLoadingLabel() {
        container.addSubview(label)
        label.addCenterHorizontallyConstraint()
        label.addBottomConstraint(LoadingIndicatorConstants.padding)
    }
    
    /// Configure container constraints
    private func setupConstraints() {
        container.addCenterHorizontallyConstraint()
        container.addCenterVerticallyConstraint()
        container.addHeightConstraint(165 + LoadingIndicatorConstants.padding)
    }
    
    /// reset loader count and hide it
    private func reset() {
        displayCount = 1
        hide()
    }
    
    // MARK: - Instance Methods
    
    /// Show loader
    /// - Parameter loadingText: loading text
    func show(loadingText: String = R.string.localizable.loading_general()) {
        displayCount += 1
        backupTimer = Timer.scheduledTimer(withTimeInterval: 20,
                                           repeats: false,
                                           block: { [weak self] _ in
            // Backup in case of faulty calls, cap at api call timeout (Added 10 seconds to make sure indicator never hides before api call is finished)
            self?.reset()
        })
        guard let keyWindow = keyWindow else {
            return
        }
        if !isDescendant(of: keyWindow) {
            keyWindow.addSubview(self)
        }
        keyWindow.bringSubviewToFront(self)
        label.text = loadingText
        activityIndicator.startAnimating()
        UIView.animate(withDuration: LoadingIndicatorConstants.animationDuration,
                       animations: { [weak self] in self?.alpha = 1.0 }
        )
    }
    
    /// Update loading state
    /// - Parameters:
    ///   - oldState: was shown
    ///   - newState: is shown
    func loadingStateUpdate(oldState: Bool, newState: Bool) {
        if oldState == newState {
            return
        }
        if newState {
            show()
            return
        }
        hide()
    }
    
    /// Hide Loader
    /// - Parameter animated: animate hiding
    func hide(animated: Bool = true) {
        displayCount = max((displayCount - 1), 0)
        guard displayCount <= 0 else {
            return
        }
        backupTimer?.invalidate()
        if animated {
            UIView.animate(withDuration: LoadingIndicatorConstants.animationDuration,
                           animations: { [weak self] in self?.alpha = 0.0 },
                           completion: { [weak self] _ in
                            guard (self?.displayCount ?? 0) <= 0 else {
                                return
                            }
                            self?.activityIndicator.stopAnimating()
                            self?.removeFromSuperview()
                           })
            return
        }
        activityIndicator.stopAnimating()
        removeFromSuperview()
    }
    
    /// Hide without animation
    func fastHide() {
        hide(animated: false)
    }
}
