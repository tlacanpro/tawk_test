//
//  Engine.swift
//  Tawk test
//
//  Created by thomas lacan on 05/04/2021.
//

import Foundation
import CoreData

class Engine {
    static let testingMode = true

    let usersService: UsersService
    let profilesService: ProfilesService
    let imageService: ImageService
    let persistentContainer: NSPersistentContainer
    let reachabilityService = ReachabilityService()
    
    let networkClient: NetworkClient
    let loadingIndicatorManager = LoadingIndicatorManager()
    let fileDataStore: FileDataStore
    let remoteImageDataStore: RemoteImageDataStore
    
    init(persistentContainer: NSPersistentContainer) {
        self.persistentContainer = persistentContainer
        networkClient = NetworkClient(endpointMapperClass: TawkEndpointMapper.self)
        fileDataStore = FileDataStore(testEnv: false)
        imageService = ImageService(networkClient: networkClient)
        remoteImageDataStore = RemoteImageDataStore(imageService: imageService, dataStore: fileDataStore)
        usersService = UsersService(networkClient: networkClient, persistentContainer: persistentContainer)
        profilesService = ProfilesService(networkClient: networkClient, persistentContainer: persistentContainer)
        networkClient.engine = self
        
    }
    
    /// Save core data view context
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
