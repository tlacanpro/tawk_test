//
//  GithubUserViewCell.swift
//  Tawk test
//
//  Created by thomas lacan on 06/04/2021.
//

import UIKit

class GithubUserViewBasicCell: UITableViewCell, GithubUserViewCellBasicImageProtocol {
    var addedBorders: Bool?
    var previousImageTask: URLSessionDataTask?
        
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        configureSubviews()
    }
    
    /// View and subviews configured
    /// - Returns: true if configured
    func getConfigured() -> Bool {
        addedBorders ?? false
    }
    
    
    /// View and subviews configured
    /// - Parameter value: value
    func setConfigured(_ value: Bool) {
        addedBorders = value
    }
    
    /// Accessor to previous image network task
    func getPreviousImageTask() -> URLSessionDataTask? {
        previousImageTask
    }
    
    /// Settter to previous image network task
    /// - Parameter value: network task
    func setPreviousImageTask(_ value: URLSessionDataTask?) {
        previousImageTask = value
    }
}
