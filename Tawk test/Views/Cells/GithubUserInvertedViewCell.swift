//
//  GithubUserReverveViewCell.swift
//  Tawk test
//
//  Created by thomas lacan on 06/04/2021.
//

import Foundation
import UIKit

class GithubUserInvertedViewCell: UITableViewCell, GithubUserViewCellInvertedProtocol {
    var addedBorders: Bool?
    var previousImageTask: URLSessionDataTask?
    private var invertedWorkItem: DispatchWorkItem?
    private var imageToInvert: UIImage?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: UserListCellType.inverted.rawValue)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        configureSubviews()
    }
    
    /// View and subviews configured
    /// - Returns: true if configured
    func getConfigured() -> Bool {
        addedBorders ?? false
    }
    
    /// View and subviews configured
    /// - Parameter value: value
    func setConfigured(_ value: Bool) {
        addedBorders = value
    }
    
    /// Accessor to previous image network task
    func getPreviousImageTask() -> URLSessionDataTask? {
        previousImageTask
    }
    
    /// Settter to previous image network task
    /// - Parameter value: network task
    func setPreviousImageTask(_ value: URLSessionDataTask?) {
        previousImageTask = value
    }
    
    /// Accessor to inverted image
    func getImageToInvert() -> UIImage? {
        imageToInvert
    }
    
    /// Setter for inverted image
    /// - Parameter value: inverted image
    func setImageToInvert(_ value: UIImage?) {
        imageToInvert = value
    }
    
    /// Accessor to inverted image work item
    func getInvertedWorkItem() -> DispatchWorkItem? {
        invertedWorkItem
    }
    
    /// Setter to inverted image work item
    /// - Parameter value: inverted image work item
    func setInvertedWorkItem(_ value: DispatchWorkItem?) {
        invertedWorkItem = value
    }
}
