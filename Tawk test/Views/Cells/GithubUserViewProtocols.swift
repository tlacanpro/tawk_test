//
//  GithubUserViewCell.swift
//  Tawk test
//
//  Created by thomas lacan on 06/04/2021.
//

import Foundation
import UIKit

protocol GithubUserViewCellProtocol: UITableViewCell {
    /// Accessor to know if cell and its subviews have been configured
    func getConfigured() -> Bool
    
    
    /// Setter to configure to know  if cell and its subviews have been configured
    /// - Parameter value: value
    func setConfigured(_ value: Bool)
    
    
    /// Accessor to previous image network task
    func getPreviousImageTask() -> URLSessionDataTask?
    
    
    /// Settter to previous image network task
    /// - Parameter value: network task
    func setPreviousImageTask(_ value: URLSessionDataTask?)
    
    
    /// Configure subviews
    func configureSubviews()
    
    /// Load image for user
    /// - Parameters:
    ///   - imageManager: image manager
    ///   - user: user to load image
    func loadImage(imageManager: RemoteImageDataStore, user: GithubUserModel)
    
    
    /// Fill cell and subviews with user data
    /// - Parameters:
    ///   - imageManager: image manager
    ///   - user: user data
    func fillWith(imageManager: RemoteImageDataStore, user: GithubUserModel)
}

extension GithubUserViewCellProtocol {
    /// Fill celll with user
    /// - Parameters:
    ///   - imageManager: image manager to download image
    ///   - user: user data
    func fillWith(imageManager: RemoteImageDataStore, user: GithubUserModel) {
        loadImage(imageManager: imageManager, user: user)
        textLabel?.text = user.login
    }
    
    /// Configure cell subviews
    func configureSubviews() {
        if !getConfigured() {
            addBorders(edges: [.all], color: .black)
            imageView?.image = UIImage(systemName: "person")
        }
        setConfigured(true)
        
        textLabel?.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        selectionStyle = .none
        
        
        imageView?.frame = CGRect(x: Constants.margin, y: Constants.margin, width: UserListViewController.kImageHeight, height: UserListViewController.kImageHeight)
        imageView?.layer.cornerRadius = UserListViewController.kImageHeight / 2
        imageView?.layer.masksToBounds = true
        imageView?.contentMode = .scaleAspectFit
        
        textLabel?.frame = CGRect(x: UserListCellType.kUserCellHeight,
                                  y: 0,
                                  width: frame.width - UserListCellType.kUserCellHeight,
                                  height: UserListCellType.kUserCellHeight)
    }
}

protocol GithubUserViewCellBasicImageProtocol: GithubUserViewCellProtocol {
    
}
extension GithubUserViewCellBasicImageProtocol {
    /// Load image for cell
    /// - Parameters:
    ///   - imageManager: image manager to get cell
    ///   - user: user whose image is loaded
    func loadImage(imageManager: RemoteImageDataStore, user: GithubUserModel) {
        imageView?.image = UIImage(systemName: "person")
        getPreviousImageTask()?.cancel()
        if let urlString = user.avatarUrl, let url = URL(string: urlString) {
            imageManager.image(for: url) { [weak self](image, url) in
                if image == nil {
                    self?.setPreviousImageTask( imageManager.downloadImageForUrl(url, completionHandler: { [weak self](image, _) in
                            self?.imageView?.image = image
                    }))
                    return
                }
                self?.imageView?.image = image
            }
        }
    }
}


protocol GithubUserViewCellInvertedProtocol: GithubUserViewCellProtocol {
    /// Accessor to inverted image
    func getImageToInvert() -> UIImage?
    
    /// Setter for inverted image
    /// - Parameter value: inverted image
    func setImageToInvert(_ value: UIImage?)
    
    
    /// Accessor to inverted image work item
    func getInvertedWorkItem() -> DispatchWorkItem?
    
    
    /// Setter to inverted image work item
    /// - Parameter value: inverted image work item
    func setInvertedWorkItem(_ value: DispatchWorkItem?)
}

extension GithubUserViewCellInvertedProtocol {
    
    /// Load user image and invert it
    /// - Parameters:
    ///   - imageManager: image manager
    ///   - user: user to load image
    func loadImage(imageManager: RemoteImageDataStore, user: GithubUserModel) {
        imageView?.image = UIImage(systemName: "person")
        // cancel the image task
        getPreviousImageTask()?.cancel()
        setImageToInvert(nil)
        getInvertedWorkItem()?.cancel()

        setInvertedWorkItem(DispatchWorkItem { [weak self] in
            guard let image = self?.getImageToInvert() else { return }
            guard let invertedImage = image.invertedImage() else { return }
            DispatchQueue.main.async {[weak self] in
                if image == self?.getImageToInvert() {
                    self?.imageView?.image = invertedImage
                    if let urlString = user.avatarUrl, let url = URL(string: urlString) {
                        imageManager.saveImage(invertedImage, from: url)
                    }
                }
            }
        })

        if let urlString = user.avatarUrl, let url = URL(string: urlString) {
            imageManager.image(for: url) { [weak self](image, url) in
                if image == nil {
                    self?.setPreviousImageTask( imageManager.downloadImageForUrl(url, store: false, completionHandler: { [weak self](image, _) in
                        
                        guard let strongSelf = self else { return }
                        strongSelf.setImageToInvert(image)
                        guard let invertedWorkItem = strongSelf.getInvertedWorkItem() else { return }
                        
                        DispatchQueue.global(qos: .background).async(execute: invertedWorkItem)
                    }))
                    return
                }
                self?.imageView?.image = image
            }
        }
    }
}

protocol GithubUserViewCellNoteProtocol: GithubUserViewCellProtocol {
    
}

extension GithubUserViewCellProtocol where Self: UITableViewCell, Self: GithubUserViewCellNoteProtocol {
    /// Fill user with user, load image, set text + accessory view for note
    /// - Parameters:
    ///   - imageManager: image manager
    ///   - user: the user
    func fillWith(imageManager: RemoteImageDataStore, user: GithubUserModel) {
        loadImage(imageManager: imageManager, user: user)
        textLabel?.text = user.login
        accessoryView = UIImageView(image: UIImage(systemName: "text.bubble.fill"))
    }
}
