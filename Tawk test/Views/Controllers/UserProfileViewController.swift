//
//  UserProfileViewController.swift
//  Tawk test
//
//  Created by thomas lacan on 07/04/2021.
//

import Foundation
import UIKit

class UserProfileViewController: UIViewController {
    let profile: GithubProfileModel
    let user: GithubUserModel
    let engine: Engine
    let mustInvertImage: Bool
    
    @IBOutlet private weak var imageView: UIImageView!
    
    @IBOutlet private weak var followingLabel: UILabel!
    @IBOutlet private weak var followersLabel: UILabel!
    
    @IBOutlet private weak var infoView: UIView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var companyLabel: UILabel!
    @IBOutlet private weak var blogLabel: UILabel!
    @IBOutlet private weak var noteLabel: UILabel!
    @IBOutlet private weak var textView: UITextView!
    @IBOutlet private weak var saveButton: UIButton!
    
    init(engine: Engine, profile: GithubProfileModel, user: GithubUserModel, mustInvertImage: Bool) {
        self.engine = engine
        self.profile = profile
        self.user = user
        self.mustInvertImage = mustInvertImage
        super.init(nibName: "UserProfileViewController", bundle: Bundle.main)
        title = profile.name
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// Configure subviews
    override func viewDidLoad() {
        super.viewDidLoad()
        configureImage()
        configureTexts()
        configureTextView()
        infoView.addBorders(edges: [.all], color: .black)
    }
    
    /// Configure text view and fill with related data
    func configureTextView() {
        textView.delegate = self
        if profile.note.isNilOrEmpty {
            textView.text = R.string.localizable.profile_notes()
            textView.textColor = .lightGray
        } else {
            textView.textColor = .black
            textView.text = profile.note ?? ""
        }
        textView.delegate = self
    }
    
    /// Configure labels + button, fill with right texts
    func configureTexts() {
        followingLabel.text = "\(R.string.localizable.profile_following()): \(profile.following ?? 0)"
        followersLabel.text = "\(R.string.localizable.profile_followers()): \(profile.followers ?? 0)"
        nameLabel.text = "\(R.string.localizable.profile_name()): \(profile.name ?? R.string.localizable.common_unknown())"
        companyLabel.text = "\(R.string.localizable.profile_company()): \(profile.company ?? R.string.localizable.common_unknown())"
        blogLabel.text = "\(R.string.localizable.profile_blog()): \(profile.blog ?? R.string.localizable.common_unknown())"
        noteLabel.text = "\(R.string.localizable.profile_notes()):"
        followingLabel.adjustsFontSizeToFitWidth = true
        followersLabel.adjustsFontSizeToFitWidth = true
        saveButton.setTitle(R.string.localizable.save_button(), for: .normal)
        saveButton.isEnabled = false
    }
    
    /// Load user related image
    func configureImage() {
        imageView.contentMode = .scaleAspectFit
        imageView?.image = UIImage(systemName: "person")
        if let urlString = user.avatarUrl, let url = URL(string: urlString) {
            engine.remoteImageDataStore.image(for: url) { [weak self](image, url) in
                if image == nil {
                    self?.engine.remoteImageDataStore.downloadImageForUrl(url, completionHandler: { [weak self](image, _) in
                            self?.imageView?.image = image
                    })
                    return
                }
                self?.imageView?.image = self?.mustInvertImage ?? false ? image?.invertedImage() : image
            }
        }
    }
    
    /// Save button touch save, save note and save related profile in DB
    /// - Parameter sender: sender
    @IBAction func saveTouchUpInside(_ sender: Any) {
        saveButton.isEnabled = false
        let index = engine.profilesService.profiles.firstIndex(where: { $0.login == profile.login }) ?? 0
        engine.profilesService.profiles[index].note = textView.text
        
        // Manage placeholder value
        if engine.profilesService.profiles[index].note == R.string.localizable.profile_notes() {
            engine.profilesService.profiles[index].note = nil
        }
        engine.profilesService.saveProfile(engine.profilesService.profiles[index], login: profile.login)
        navigationController?.popViewController(animated: true)
    }
}

extension UserProfileViewController: UITextViewDelegate {
    /// Manage placeholder for text view change style when typing
    /// - Parameter textView: text view
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = .black
        }
    }
    
    /// Manage placeholder for text view change style when end typing
    /// - Parameter textView: text view
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.textColor = UIColor.lightGray
            textView.text = R.string.localizable.profile_notes()
        }
        saveButton.isEnabled = textView.text != profile.note
    }
}
