//
//  ViewController.swift
//  Tawk test
//
//  Created by thomas lacan on 05/04/2021.
//

import UIKit
import Combine

enum UserListCellType: String {
    static let kUserCellHeight: CGFloat = UserListViewController.kImageHeight + Constants.margin * 2
    
    case basic = "userListCellTypeBasic"
    case inverted = "userListCellTypeInverted"
    case note = "userListCellTypeNote"
    case invertedWithNote = "userListCellTypeBasicInvertedNote"
    case spacer = "spacerCell"
    
    
    /// cell at index path
    /// - Parameters:
    ///   - engine: app Engine
    ///   - data: all users data to display
    ///   - tableView: table view for cell
    ///   - indexPath: indexPath to display
    /// - Returns: cell fill and configured with data
    func cell(engine: Engine, data: [GithubUserModel], tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.rawValue, for: indexPath)
        if let userCell = cell as? GithubUserViewCellProtocol {
            userCell.fillWith(imageManager: engine.remoteImageDataStore, user: data[indexPath.row / 2])
        }
        cell.selectionStyle = .none
        return cell
    }
    
    /// Cell height spacer or data cell
    /// - Returns: cell height
    func height() -> CGFloat {
        self == .spacer ? Constants.margin : UserListCellType.kUserCellHeight
    }
    
    /// Determine the cell type depending the parameters
    /// - Parameters:
    ///   - data: users data
    ///   - indexPath: cell index path
    ///   - engine: App engine
    /// - Returns: celll type
    static func typeAt(data: [GithubUserModel], indexPath: IndexPath, engine: Engine) -> UserListCellType {
        // spacer between rows
        if indexPath.row % 2 == 0 {
            return .spacer
        }
        
        // check user information to know the type
        let user = data[indexPath.row / 2]
        let hasNote = !(engine.profilesService.profileForUser(user)?.note.isNilOrEmpty ?? true)
        if ((indexPath.row / 2) + 1) % 4 == 0 {
            return hasNote ? .invertedWithNote : .inverted
        }
        return hasNote ? .note : .basic
    }
}

class UserListViewController: UIViewController {
    static let kImageHeight: CGFloat = 64
    
    let engine: Engine
    let refreshControl = UIRefreshControl()
    lazy var searchController = UISearchController(searchResultsController: nil)
    
    @IBOutlet private weak var noConnectionView: UIView!
    @IBOutlet private weak var noConnectionLabel: UILabel!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    private var selectedIndexPath: IndexPath?
    private var cancellable: AnyCancellable?
    var searchText: String?
    var data: [GithubUserModel]
    
    init(engine: Engine) {
        self.engine = engine
        data = engine.usersService.users
        super.init(nibName: "UserListViewController", bundle: Bundle.main)
        engine.usersService.register(observer: self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        engine.usersService.unregister(observer: self)
    }
    
    /// Configure subviews and load users data
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNoConnection()
        configureSearchController()
        activityIndicator.isHidden = true
        configureTableView()
        engine.usersService.loadUsers { [weak self](error) in
            if error == nil {
                self?.refreshData()
            }
        }
    }
    
    
    /// Refresh UI when view will appear if needed
    /// - Parameter animated: animated
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let selectedIndexPath = selectedIndexPath {
            tableView.reloadRows(at: [selectedIndexPath], with: .automatic)
            self.selectedIndexPath = nil
        }
    }

    
    /// Configure search controller and set current VC as delegate
    func configureSearchController() {
        searchController.delegate = self
        searchController.searchResultsUpdater = self
        searchController.searchBar.autocapitalizationType = .none
        searchController.searchBar.delegate = self
        navigationItem.searchController = searchController
    }
    
    /// Configure UI when no connection
    func configureNoConnection() {
        noConnectionLabel.text = R.string.localizable.no_connection()
        noConnectionView.isHidden = engine.reachabilityService.isConnected || !engine.usersService.users.isEmpty
        tableView.isHidden = !noConnectionView.isHidden
        cancellable = engine.reachabilityService.$isConnected.sink(receiveValue: { [weak self] connected in
            guard let strongSelf = self else { return }
            if connected && strongSelf.engine.usersService.users.isEmpty {
                strongSelf.loadUsers()
                strongSelf.noConnectionView.isHidden = true
                strongSelf.tableView.isHidden = false
            } else if !connected && strongSelf.engine.usersService.users.isEmpty {
                strongSelf.noConnectionView.isHidden = false
                strongSelf.tableView.isHidden = true
            }
        })
    }
    
    /// Configure table view, register all cell types
    private func configureTableView() {
        tableView.register(GithubUserViewBasicCell.self,
                           forCellReuseIdentifier: UserListCellType.basic.rawValue)
        tableView.register(GithubUserInvertedViewCell.self,
                           forCellReuseIdentifier: UserListCellType.inverted.rawValue)
        tableView.register(GithubUserNoteViewCell.self,
                           forCellReuseIdentifier: UserListCellType.note.rawValue)
        tableView.register(GithubUserInvertedNoteViewCell.self,
                           forCellReuseIdentifier: UserListCellType.invertedWithNote.rawValue)
        tableView.register(UITableViewCell.self,
                           forCellReuseIdentifier: UserListCellType.spacer.rawValue)
        tableView.separatorStyle = .none
    }
    
    /// Load users data from service, display loader and refresh UI when done
    func loadUsers() {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        let nbRows = engine.usersService.users.count * 2
        engine.usersService.loadUsers(onDone: { [weak self](error) in
            guard let strongSelf = self else { return }
            strongSelf.activityIndicator.isHidden = true
            strongSelf.activityIndicator.stopAnimating()
            if error == nil {
                strongSelf.refreshData(withReload: false)
                if nbRows == 0 {
                    strongSelf.tableView.reloadData()
                    return
                }
                let nbRowsAfter = strongSelf.engine.usersService.users.count * 2
                var reloadIndexPath: [IndexPath] = []
                var rowAddedIndex = nbRows
                while rowAddedIndex < nbRowsAfter {
                    reloadIndexPath.append(IndexPath(row: rowAddedIndex, section: 0))
                    rowAddedIndex += 1
                }
                if !reloadIndexPath.isEmpty {
                    strongSelf.tableView.beginUpdates()
                    strongSelf.tableView.insertRows(at: reloadIndexPath, with: .automatic)
                    strongSelf.tableView.endUpdates()
                }
            }
        })
    }
    
    
    /// Refresh used data after load users
    /// - Parameter withReload: reload table view
    func refreshData(withReload: Bool = true) {
        if searchText.isNilOrEmpty {
            data = engine.usersService.users
            if withReload {
                tableView.reloadData()
            }
            return
        }
        let profiles = engine.profilesService.profilesWithNoteContaining(searchText ?? "")
        var usersWithNoteMatching: [GithubUserModel] = []
        profiles.forEach({ profile in
            if let user = engine.usersService.users.first(where: { $0.login == profile.login }) {
                usersWithNoteMatching.append(user)
            }
        })
        var users = engine.usersService.users.filter({ ($0.login?.lowercased().contains(searchText?.lowercased() ?? "")) ?? false })
        usersWithNoteMatching.forEach({
            users.appendIfNotContains($0)
        })
        data = users
        if withReload {
            tableView.reloadData()
        }
    }
}

extension UserListViewController: UITableViewDelegate, UITableViewDataSource {
    /// Number of sections
    /// - Parameter tableView: table view
    /// - Returns: number
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    /// Number of rows is number of users + space
    /// - Parameters:
    ///   - tableView: table view
    ///   - section: section
    /// - Returns: number
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count * 2
    }
    
    /// Cell at index path
    /// - Parameters:
    ///   - tableView: table view
    ///   - indexPath: index path
    /// - Returns: created cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        UserListCellType
            .typeAt(data: data, indexPath: indexPath, engine: engine)
            .cell(engine: engine, data: data, tableView: tableView, indexPath: indexPath)
    }
    
    /// Cell height
    /// - Parameters:
    ///   - tableView: table view
    ///   - indexPath: index path
    /// - Returns: value
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UserListCellType.typeAt(data: data, indexPath: indexPath, engine: engine).height()
    }
    
    /// Avoid selection on spacer rows ( row % 2)
    /// - Parameters:
    ///   - tableView: table view
    ///   - indexPath: indexpath
    /// - Returns: indexPath if can select row
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        indexPath.row % 2 == 0 ? nil : indexPath
    }
    
    /// User did select a user row load related profile if needed and show it
    /// - Parameters:
    ///   - tableView: table view
    ///   - indexPath: index path
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = data[indexPath.row / 2]
        let mustInvertImage = ((engine.usersService.users.firstIndex(of: user) ?? 0) + 1) % 4 == 0
        selectedIndexPath = indexPath
        if let profile = engine.profilesService.profileForUser(user) {
            navigationController?.pushViewController(
                UserProfileViewController(engine: engine,
                                          profile: profile,
                                          user: user,
                                          mustInvertImage: mustInvertImage), animated: true)
            return
        }
        engine.loadingIndicatorManager.show()
        engine.profilesService.loadProfileFromUser(user) { [weak self](profile, error) in
            guard let strongSelf = self else { return }
            strongSelf.engine.loadingIndicatorManager.hide()
            if let error = error {
                strongSelf.presentAlert(message: error.localizedDescription)
              return
            }
            if let profile = profile {
                strongSelf.navigationController?.pushViewController(
                    UserProfileViewController(engine: strongSelf.engine,
                                              profile: profile,
                                              user: user,
                                              mustInvertImage: mustInvertImage), animated: true)
            }
        }
    }
    
    
    /// Manage lazy loading when will display bottom of table view cell
    /// - Parameters:
    ///   - tableView: table view
    ///   - cell: cell
    ///   - indexPath: index path
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !searchText.isNilOrEmpty {
            return
        }
        if indexPath.row == engine.usersService.users.count * 2 - 1 && engine.usersService.state != .loading {
            loadUsers()
        }
    }
}

extension UserListViewController: UsersServiceObserver {
    /// Subscribe to user service event, show error if needed
    /// - Parameters:
    ///   - usersService: user service
    ///   - state: new state
    func onUsersService(usersService: UsersService, didUpdate state: ServiceState) {
        switch state {
        case .error(let error):
            presentAlert(message: error.localizedDescription)
        default:
            return
        }
    }
}

extension UserListViewController: UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate {
    /// Search bar delegate, update data depending text value
    /// - Parameters:
    ///   - searchBar: search bar
    ///   - searchText: search text
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if self.searchText?.lowercased() != searchText.lowercased() {
            self.searchText = searchText
            refreshData()
        }
    }
    
    /// Not usefull search results
    /// - Parameter searchController: search controller
    func updateSearchResults(for searchController: UISearchController) {
        if searchText?.lowercased() != searchController.searchBar.searchTextField.text?.lowercased() {
            searchController.searchBar.searchTextField.text = searchText
        }
    }
}
