//
//  Constants.swift
//  Tawk test
//
//  Created by thomas lacan on 05/04/2021.
//

import UIKit

struct Constants {
    static let highestLayerIndex: Double = 9999
    static let bottomSafeArea: CGFloat = UIApplication.shared.windows.first(where: { $0.isKeyWindow })?.safeAreaInsets.bottom ?? 0.0
    static let topSafeArea: CGFloat = UIApplication.shared.windows.first(where: { $0.isKeyWindow })?.safeAreaInsets.top ?? 0.0
    static let screenWidth: CGFloat = UIScreen.main.bounds.size.width
    static let screenHeight: CGFloat = UIScreen.main.bounds.size.height
    static let screenHeightContent: CGFloat = screenHeight - (bottomSafeArea + topSafeArea)
    static let margin: CGFloat = 16.0
    static let spacing: CGFloat = 32.0
}
