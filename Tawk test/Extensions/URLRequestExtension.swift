//
//  URLRequestExtension.swift
//  Tawk test
//
//  Created by thomas lacan on 05/04/2021.
//

import Foundation

extension URLRequest {
    
    /// Provide curl request for url request
    public var curlString: String {
        #if RELEASE
        return ""
        #else
        var result = "curl -k "
        
        if let method = httpMethod {
            result += "-X \(method) \\\n"
        }
        
        if let headers = allHTTPHeaderFields {
            for (header, value) in headers {
                result += "-H \"\(header): \(value)\" \\\n"
            }
        }
        
        if let body = httpBody, !body.isEmpty, let string = String(data: body, encoding: .utf8), !string.isEmpty {
            result += "-d '\(string)' \\\n"
        }
        
        if let url = url {
            result += url.absoluteString
        }
        
        return result
        #endif
    }
}
