//
//  UIViewExtensions.swift
//  Tawk test
//
//  Created by thomas lacan on 05/04/2021.
//

import UIKit

extension UIView {
    
    var allSubviews: [UIView] {
         subviews.flatMap { [$0] + $0.allSubviews }
    }
    
    /// Add bottom constraint view to superview
    /// - Parameter constant: distance between superview and view
    /// - Returns: created constraint
    @discardableResult func addBottomConstraint(_ constant: CGFloat) -> NSLayoutConstraint? {
        guard let superview = superview else {
            return nil
        }
        translatesAutoresizingMaskIntoConstraints = false

        let constraint = NSLayoutConstraint(item: self,
                                            attribute: .bottom,
                                            relatedBy: .equal,
                                            toItem: superview,
                                            attribute: .bottom,
                                            multiplier: 1.0,
                                            constant: -constant)
        superview.addConstraint(constraint)

        return constraint
    }
    
    /// Add top constraint view to superview
    /// - Parameter constant: distance between superview and view
    /// - Returns: created constraint
    @discardableResult func addTopConstraint(_ constant: CGFloat) -> NSLayoutConstraint? {
        guard let superview = superview else {
            return nil
        }
        translatesAutoresizingMaskIntoConstraints = false

        let constraint = NSLayoutConstraint(item: self,
                                            attribute: .top,
                                            relatedBy: .equal,
                                            toItem: superview,
                                            attribute: .top,
                                            multiplier: 1.0,
                                            constant: constant)
        superview.addConstraint(constraint)

        return constraint
    }
    
    
    /// Add width equal equal constaint to view
    /// - Parameter constant: width value
    /// - Returns: created constraint
    @discardableResult func addWidthConstraint(_ width: CGFloat) -> NSLayoutConstraint? {
        guard let superview = superview else {
            return nil
        }
        translatesAutoresizingMaskIntoConstraints = false
        // Width
        let widthC = NSLayoutConstraint(
            item: self,
            attribute: .width,
            relatedBy: .equal,
            toItem: nil,
            attribute: .notAnAttribute,
            multiplier: 1.0,
            constant: width)

        superview.addConstraint(widthC)

        return widthC
    }
    
    /// Add height equal equal constaint to view
    /// - Parameter constant: width value
    /// - Returns: created constraint
    @discardableResult func addHeightConstraint(_ height: CGFloat) -> NSLayoutConstraint? {
        guard let superview = superview else {
            return nil
        }
        translatesAutoresizingMaskIntoConstraints = false

        let heightC = NSLayoutConstraint(
            item: self,
            attribute: .height,
            relatedBy: .equal,
            toItem: nil,
            attribute: .notAnAttribute,
            multiplier: 1.0,
            constant: height)

        superview.addConstraint(heightC)

        return heightC
    }

    
    /// Add left constraint view to superview
    /// - Parameter constant: distance between superview and view
    /// - Returns: created constraint
    @discardableResult func addLeftConstraint(_ constant: CGFloat) -> NSLayoutConstraint? {
        guard let superview = superview else {
            return nil
        }
        translatesAutoresizingMaskIntoConstraints = false

        let constraint = NSLayoutConstraint(item: self,
                                            attribute: .leading,
                                            relatedBy: .equal,
                                            toItem: superview,
                                            attribute: .leading,
                                            multiplier: 1.0,
                                            constant: constant)
        superview.addConstraint(constraint)

        return constraint
    }

    /// Add right constraint view to superview
    /// - Parameter constant: distance between superview and view
    /// - Returns: created constraint
    @discardableResult func addRightConstraint(_ constant: CGFloat) -> NSLayoutConstraint? {
        guard let superview = superview else {
            return nil
        }
        translatesAutoresizingMaskIntoConstraints = false

        let constraint = NSLayoutConstraint(item: self,
                                            attribute: .trailing,
                                            relatedBy: .equal,
                                            toItem: superview,
                                            attribute: .trailing,
                                            multiplier: 1.0,
                                            constant: -constant)
        superview.addConstraint(constraint)

        return constraint
    }
    
    
    /// Add center constraint between view and super view Horizontally
    func addCenterHorizontallyConstraint() {
        addCenterConstraint(with: .centerX)
    }

    /// Add center constraint between view and super view Vertically
    func addCenterVerticallyConstraint() {
        addCenterConstraint(with: .centerY)
    }

    
    /// Add center constraint between view and super view
    /// - Parameter attribute: center position
    private func addCenterConstraint(with attribute: NSLayoutConstraint.Attribute) {
        guard let superview = superview else { return }
        translatesAutoresizingMaskIntoConstraints = false

        let centerC = NSLayoutConstraint(
            item: self,
            attribute: attribute,
            relatedBy: .equal,
            toItem: superview,
            attribute: attribute,
            multiplier: 1.0,
            constant: 0)

        superview.addConstraint(centerC)
    }
    
    
    /// Add borders around views
    /// - Parameters:
    ///   - edges: positions to add border
    ///   - color: border color
    ///   - onlyLeftInset: add inset only on left side
    ///   - inset: inset distance value for border
    ///   - thickness: border size
    /// - Returns: array of created borders views
    @discardableResult
    func addBorders(edges: UIRectEdge,
                    color: UIColor,
                    onlyLeftInset: Bool? = false,
                    inset: CGFloat = 0.0,
                    thickness: CGFloat = 1.0) -> [UIView] {
        
        var borders = [UIView]()
        
        @discardableResult
        func addBorder(formats: String...) -> UIView {
            let border = UIView(frame: .zero)
            border.backgroundColor = color
            border.translatesAutoresizingMaskIntoConstraints = false
            addSubview(border)
            addConstraints(formats.flatMap {
                NSLayoutConstraint.constraints(withVisualFormat: $0,
                                               options: [],
                                               metrics: ["inset": inset, "thickness": thickness],
                                               views: ["border": border])
            })
            borders.append(border)
            return border
        }
        
        
        if edges.contains(.top) || edges.contains(.all) {
            if onlyLeftInset == true {
                addBorder(formats: "V:|-0-[border(==thickness)]", "H:|-inset-[border]|")
            } else {
                addBorder(formats: "V:|-0-[border(==thickness)]", "H:|-inset-[border]-inset-|")
            }
        }
        
        if edges.contains(.bottom) || edges.contains(.all) {
            if onlyLeftInset == true {
                addBorder(formats: "V:[border(==thickness)]-0-|", "H:|-inset-[border]|")
            } else {
                addBorder(formats: "V:[border(==thickness)]-0-|", "H:|-inset-[border]-inset-|")
            }
        }
        
        if edges.contains(.left) || edges.contains(.all) {
            if onlyLeftInset == true {
                addBorder(formats: "V:|-inset-[border]|", "H:|-0-[border(==thickness)]")
            } else {
                addBorder(formats: "V:|-inset-[border]-inset-|", "H:|-0-[border(==thickness)]")
            }
        }
        
        if edges.contains(.right) || edges.contains(.all) {
            if onlyLeftInset == true {
                addBorder(formats: "V:|-inset-[border]|", "H:[border(==thickness)]-0-|")
            } else {
                addBorder(formats: "V:|-inset-[border]-inset-|", "H:[border(==thickness)]-0-|")
            }
        }
        
        return borders
    }
}
