//
//  UIImageExtensions.swift
//  Tawk test
//
//  Created by thomas lacan on 06/04/2021.
//

import Foundation
import UIKit

extension UIImage {
    
    /// Invert image colors using CIColorInvert filter
    /// - Returns: inverted image
    func invertedImage() -> UIImage? {
        guard let currentCGImage = self.cgImage else { return nil }
               let currentCIImage = CIImage(cgImage: currentCGImage)
               let filter = CIFilter(name: "CIColorInvert")
               filter?.setValue(currentCIImage, forKey: "inputImage")
               guard let outputImage = filter?.outputImage else { return nil }

               let context = CIContext()

               if let cgimg = context.createCGImage(outputImage, from: outputImage.extent) {
                   return UIImage(cgImage: cgimg)
               }
               return nil
    }
    
}
