//
//  DataExtension.swift
//  Tawk test
//
//  Created by thomas lacan on 05/04/2021.
//

import Foundation

extension Data {
    
    /// Convert Json into Codable instance
    /// - Parameter jsonDict: json values dic
    /// - Throws: Decoding errors
    /// - Returns: Codable instance
    static func jsonToCodable<T>(from jsonDict: [String: Any]) throws -> T where T: Codable {
        let decoder = JSONDecoder()
        let data = try JSONSerialization.data(
            withJSONObject: jsonDict,
            options: []
        )
        return try decoder.decode(T.self, from: data)
    }
    
    /// Convert Json array into Codable instance
    /// - Parameter jsonArray: json array values
    /// - Throws: Decoding errors
    /// - Returns: Codable instance
    static func jsonToCodable<T>(from jsonArray: [[String: Any]]) throws -> [T] where T: Codable {
        let decoder = JSONDecoder()
        let data = try JSONSerialization.data(
            withJSONObject: jsonArray,
            options: []
        )
        return try decoder.decode([T].self, from: data)
    }
    
    /// Data to Codable instance
    /// - Returns: Codable instance
    func modelFromData<T>() -> T? where T: Codable {
        do {
            if let responseDict = try mapJSON() as? [String: Any],
               let model: T = try Data.jsonToCodable(from: responseDict) {
                return model
            }
        } catch let error {
            print("[Model] error \(error.localizedDescription)")
        }
        return nil
    }
    
    /// Data to array of Codable instance
    /// - Returns: Array of Codable instance
    func arrayModelFromData<T>() -> [T]? where T: Codable {
        do {
            let result: [T] = try JSONDecoder().decode([T].self, from: self)
            return result
        } catch let error {
            print("[Model] error \(error.localizedDescription)")
        }
        return nil
    }
    
    /// Data to Dic
    /// - Returns: Data related dic
    func mapJSON(failsOnEmptyData: Bool = true) throws -> Any {
        do {
            return try JSONSerialization.jsonObject(with: self, options: .allowFragments)
        } catch {
            if count < 1 && !failsOnEmptyData {
                return NSNull()
            }
            throw NSError(domain: "Data.mapJSON", code: 1, userInfo: nil)
        }
    }
}
