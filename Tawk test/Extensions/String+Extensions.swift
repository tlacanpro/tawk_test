//
//  String+Extensions.swift
//  Tawk test
//
//  Created by thomas lacan on 06/04/2021.
//

import Foundation
import UIKit
import CommonCrypto

extension String {
    /// Sha 256 encoded value
    /// - Returns: encoded value
    func sha256() -> String {
        if let stringData = self.data(using: String.Encoding.utf8) {
            return hexStringFromData(input: digest(input: stringData as NSData))
        }
        return ""
    }
    
    /// Creates sha256 disgest
    /// - Parameter input: data to digest
    /// - Returns: data digested
    private func digest(input: NSData) -> NSData {
        let digestLength = Int(CC_SHA256_DIGEST_LENGTH)
        var hash = [UInt8](repeating: 0, count: digestLength)
        CC_SHA256(input.bytes, UInt32(input.length), &hash)
        return NSData(bytes: hash, length: digestLength)
    }
    
    /// data to hex value
    /// - Parameter input: data to convert in hex format
    /// - Returns: hex value
    private func hexStringFromData(input: NSData) -> String {
        var bytes = [UInt8](repeating: 0, count: input.length)
        input.getBytes(&bytes, length: input.length)
        
        var hexString = ""
        for byte in bytes {
            hexString += String(format: "%02x", UInt8(byte))
        }
        
        return hexString
    }
}

extension Optional where Wrapped == String {
    
    /// Determine if string is nil or empty
    var isNilOrEmpty: Bool {
        switch self {
        case .none: return true
        case .some(let s): return s.isEmpty
        }
    }
}
