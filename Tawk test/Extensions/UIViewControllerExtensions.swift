//
//  UIViewControllerExtensions.swift
//  Tawk test
//
//  Created by thomas lacan on 07/04/2021.
//

import Foundation
import UIKit

extension UIViewController {
    
    /// present alert with provided message and without title
    /// - Parameter message: alert message
    func presentAlert(message: String) {
        presentAlert(title: "", message: message)
    }
    
    /// Present alert with provided parameters
    /// - Parameters:
    ///   - title: alert title
    ///   - message: alert message
    ///   - completion: handler for ok button pressed
    func presentAlert(title: String, message: String?, completion: (() -> Void)? = nil) {
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ac.addButton(title: R.string.localizable.ok_button(), style: .default, handler: nil)
        self.present(ac, animated: true, completion: completion)
    }
    
}

extension UIAlertController {
    
    /// Add button to alert
    /// - Parameters:
    ///   - title: button title
    ///   - style: alert button style
    ///   - handler: button handler when touch up
    func addButton(title: String, style: UIAlertAction.Style, handler: ((UIAlertAction) -> Void)?) {
        let action = UIAlertAction(title: title, style: style, handler: handler)
        addAction(action)
    }
}
