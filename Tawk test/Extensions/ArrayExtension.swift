//
//  ArrayExtension.swift
//  Tawk test
//
//  Created by thomas lacan on 10/04/2021.
//

import Foundation

extension Array where Element: Equatable {
    /// Add element in array if not already into it
    /// - Parameter element: element to add
    /// - Returns: true if element added
    @discardableResult
    mutating func appendIfNotContains(_ element: Element) -> Bool {
        if !contains(element) {
            append(element)
            return true
        }
        return false
    }
    
    /// Add element in array if contains it
    /// - Parameter element: element to remove
    /// - Returns: true if element removed and was in array
    @discardableResult
    mutating func removeElement(_ element: Element) -> Bool {
        if let index = firstIndex(of: element) {
            remove(at: index)
            return true
        }
        return false
    }
    
    /// Remove Elements duplications in array
    /// - Returns: array without duplication
    func removeDuplicates() -> [Element] {
        var result = [Element]()

        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }

        return result
    }
}
