//
//  GithubProfileModel.swift
//  Tawk test
//
//  Created by thomas lacan on 06/04/2021.
//

import Foundation
import CoreData

struct GithubProfileModel: Codable, Equatable {
    var login: String?
    let name: String?
    let company: String?
    let blog: String?
    let email: String?
    let followers: Int?
    let following: Int?
    var note: String?
}

class GithubProfileModelDB: NSManagedObject {
    @NSManaged var login: String
    @NSManaged var name: String?
    @NSManaged var company: String?
    @NSManaged var blog: String?
    @NSManaged var email: String?
    @NSManaged var followers: Int
    @NSManaged var following: Int
    @NSManaged var note: String?
    
    /// Fil Profile DB profile with Codable profile
    /// - Parameter structProfile: Codable profile
    func fill(with structProfile: GithubProfileModel) {
        self.login = structProfile.login ?? "Unknown"
        self.name = structProfile.name
        self.company = structProfile.company
        self.blog = structProfile.blog
        self.email = structProfile.email
        self.followers = structProfile.followers ?? 0
        self.following = structProfile.following ?? 0
        self.note = structProfile.note
    }

    
    /// Codable profile from DB profile
    /// - Returns: Codable profile
    func githubProfileModel() -> GithubProfileModel {
        GithubProfileModel(login: login, name: name,
                           company: company,
                           blog: blog,
                           email: email,
                           followers: followers,
                           following: following,
                           note: note)
    }
}
