//
//  GithubUserModel.swift
//  Tawk test
//
//  Created by thomas lacan on 05/04/2021.
//

// MARK: - Element
import Foundation
import CoreData

struct GithubUserModel: Codable, Equatable {
    let login: String?
    let id: Int
    let avatarUrl: String?
    let url, htmlUrl, followersUrl: String?
    let followingUrl, gistsUrl, starredUrl: String?
    let subscriptionsUrl, organizationsUrl, reposUrl: String?
    let eventsUrl: String?
    let receivedEventsUrl: String?
    let type: GithubUserType?
    let siteAdmin: Bool?
    
    enum CodingKeys: String, CodingKey {
            case login, id
            case avatarUrl = "avatar_url"
            case url
            case htmlUrl = "html_url"
            case followersUrl = "followers_url"
            case followingUrl = "following_url"
            case gistsUrl = "gists_url"
            case starredUrl = "starred_url"
            case subscriptionsUrl = "subscriptions_url"
            case organizationsUrl = "organizationsurl"
            case reposUrl = "repos_url"
            case eventsUrl = "events_url"
            case receivedEventsUrl = "received_events_url"
            case type
            case siteAdmin = "site_admin"
        }
    
    
}

class GithubUserModelDB: NSManagedObject {
    @NSManaged private var login: String?
    @NSManaged private var id: Int
    @NSManaged private var avatarURL: String?
    @NSManaged private var url, htmlURL, followersURL: String?
    @NSManaged private var followingURL, gistsURL, starredURL: String?
    @NSManaged private var subscriptionsURL, organizationsURL, reposURL: String?
    @NSManaged private var eventsURL: String?
    @NSManaged private var receivedEventsURL: String?
    @NSManaged private var type: String?
    @NSManaged private var siteAdmin: Bool
    
    /// Fill the properties of DB user with Codable user
    /// - Parameter structUser: Codable user
    func fill(with structUser: GithubUserModel) {
        self.login = structUser.login
        self.id = structUser.id
        self.avatarURL = structUser.avatarUrl
        self.url = structUser.url
        self.htmlURL = structUser.htmlUrl
        self.followersURL = structUser.followersUrl
        self.followingURL = structUser.followingUrl
        self.gistsURL = structUser.gistsUrl
        self.starredURL = structUser.starredUrl
        self.subscriptionsURL = structUser.subscriptionsUrl
        self.organizationsURL = structUser.organizationsUrl
        self.reposURL = structUser.reposUrl
        self.eventsURL = structUser.eventsUrl
        self.receivedEventsURL = structUser.receivedEventsUrl
        self.type = structUser.type?.rawValue ?? GithubUserType.user.rawValue
        self.siteAdmin = structUser.siteAdmin ?? false
    }
    
    /// Codable user from DB user
    /// - Returns: Codable user
    func githubUserModel() -> GithubUserModel {
        GithubUserModel(login: login,
                        id: id,
                        avatarUrl: avatarURL,
                        url: url,
                        htmlUrl: htmlURL,
                        followersUrl: followersURL,
                        followingUrl: followingURL,
                        gistsUrl: gistsURL,
                        starredUrl: starredURL,
                        subscriptionsUrl: subscriptionsURL,
                        organizationsUrl: organizationsURL,
                        reposUrl: reposURL,
                        eventsUrl: eventsURL,
                        receivedEventsUrl: receivedEventsURL,
                        type: GithubUserType(rawValue: type ?? GithubUserType.user.rawValue),
                        siteAdmin: siteAdmin)
    }
}

enum GithubUserType: String, Codable {
    case organization = "Organization"
    case user = "User"
}
